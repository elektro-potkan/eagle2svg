Eagle2SVG
==========
**Eagle schematic exporting tool**<br>
This ULP can convert an Eagle CAD schematics into SVG files.

Load any schematic and execute the ULP in the Eagle.

Known issues in board export:
- elongated pads are not supported
- layers are not placed correctly

Based on the work by:
- xtitoris <xtitoris@gmail.com>
- Nils Springob <nils.springob@nicai-systems.de>


Usage
-----
Run the ULP from schematic or board editor.
You can then select if it should be exported as monochrome and if you want to export only visible layers or all of them.
For schematic, you could also select the sheet you want to export.


It supports passing of the options as following arguments on Eagle's command-line:
* "**--cli**" = do not open any dialog - run as CLI (command-line interface) version  
	(default is to use the GUI)

* "**--board**" = use board editor independently of currently active one

* "**--schematic**" = use schematic editor independently of currently active one

* "**--mirror**" = export it mirrored

* "**--monochrome**" = export it as black-and-white  
	(default is color version)

* "**--sheet &lt;number&gt;**" = for schematic, export the selected sheet  
	(by default, the active (opened/last opened) one will be exported - this is OK for single-sheet schematics, but ambiguous for the others)

* "**--sheets-all**" = export all sheets of schematic

* "**--layers &lt;list-of-layers&gt;**" = export specified layers only  
	Takes list of layers to export in the form of comma-separated layers' numbers (no space allowed).

* "**--layers-visible**" = export visible layers only  
	(default option)

* "**--layers-all**" = export also hidden layers  
	(supersedes older **--visibility-all** argument, which is now deprecated)

* "**--output &lt;path&gt;**" = output file-path
	
	The default ones are:
	- "&lt;name&gt;_brd.svg" for boards
	- "&lt;name&gt;_p&lt;sheet-number&gt;.svg" for single sheet of schematic
	- "&lt;name&gt;_p#.svg" for all schematic sheets
	
	When exporting all sheets, the file-path must contain at least single hash ('#') character.
	The last one in the file-path will be replaced by the sheet number.
	It is also possible to specify more of these "place-holders" - the sheet number will be formatted for at least that number of places
	(e.g. for "example_##.svg", the sheets will be exported as "example_01.svg", "example_02.svg", etc.).


You could pass the arguments into the Eagle command-line:
```
RUN eagle2svg.ulp --sheet 1 --monochrome
```
The arguments ar parsed first, then the GUI dialog will appear (respecting those arguments).

This is not that useful, but you could run the script from outside of Eagle, which then makes it quite powerful:
```bash
eagle -C "RUN eagle2svg.ulp --cli --sheets-all --output 'example-##.svg'; QUIT;" example.sch
```
Which in turn opens the schematic file "example.sch" in Eagle, exports all its sheets into files "example-01.svg", "example-02.svg", etc. and closes the Eagle after that.

Because of the common problem with the order of execution, when Eagle opens the specified file first,
then opens the associated board/schematic and after that runs the ULP (usually on the wrong editor window),
it is highly recommended that you specify the desired one when running in CLI mode:
```bash
eagle -C "RUN eagle2svg.ulp --cli --schematic --sheets-all --layers 91,92,94 --output 'example-##.svg'; QUIT;" example.sch
```


License
-------
This program is licensed under the GNU General Public License version 2 (GNU GPLv2).

See file [LICENSE](LICENSE).

> This program is free software; you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation; version 2 of the License.
> 
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
> 
> You should have received a copy of the GNU General Public License along
> with this program; if not, write to the Free Software Foundation, Inc.,
> 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
